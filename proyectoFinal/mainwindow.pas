unit mainwindow;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, uCmdBox,
   SpkToolbar, spkt_Tab, spkt_Pane, spkt_Buttons, CmdParser, mCalculator,
   mMatrix,
   {FORMS} formAddConst, formGraph,
   {SOL1Eqution - closeMethods} bisectionmethod, FalsePositionMethod,
   {SOL1Eqution - openMethods} NewtonMethod, secantMethod, FixPointMethod,
   {Interpolation} lagrangePol, newtonPol,
   {newtonGeneralizado} NewtonGenMethod,
   {ODE} RungeKMethod, HeunMethod, EulerMethod, DormandPMethod, BaseMethod,
   {INTEGRAL} Simpson1_3Method, Simpson3_8Method, TrapezoidalMethod, AreaMethod;

type

   { TmainForm }

   TmainForm = class(TForm)
      cmdBox: TCmdBox;
      btnAddConst: TSpkLargeButton;
      btnSaveHistory: TSpkLargeButton;
      btnLoadHistory: TSpkLargeButton;
      SpkPane1: TSpkPane;
      SpkPane2: TSpkPane;
      SpkTab1: TSpkTab;
      SpkTab2: TSpkTab;
      SpkToolbar1: TSpkToolbar;
      procedure btnSaveHistoryClick(Sender: TObject);
      procedure cmdBoxInput(aCmdBox: TCmdBox; input: string);
      procedure FormCreate(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
      procedure btnAddConstClick(Sender: TObject);
   private
      mathParser : TCalculator;
      cmdParser : TCmdParser;

      oneEqBisection : TBisectionMethod;
      oneEqFalsePosition : TFalsePositionMethod;
      oneEqNewton : TNewtonMethod;
      oneEqSecant : TSecantMethod;
      oneEqFixPoint : TFixPointMethod;

      polLagrange: TLagrangePol;
      polNewton : TNewtonPol;

      genNewton : TNewtonGenMethod;

      odeRungeK : TRungeKuttaMethod;
      odeHeun : THeunMethod;
      odeEuler : TEulerMethod;
      odeDormandP : TDormandPrinceMethod;

      itgSimpson1_3 : TSimpson1_3Method;
      itgSimpson3_8 : TSimpson3_8Method;
      itgTrapezoidal : TTrapezoidalMethod;

      areaCalc : TAreaMethod;
      procedure startCommand();
      function myStrToFloat(str: string) : real;
      function matrixToStr(constref input : TMatrix) : string;


   public

      { public declarations }
   end;
const
   formatF = '0.########';
var
   mainForm: TmainForm;
   ans : string ;// valor anterior
implementation

{$R *.lfm}

{ TmainForm }

procedure TmainForm.FormCreate(Sender: TObject);
begin
   mathParser := TCalculator.create();
   cmdParser := TCmdParser.create();
   cmdBox.TextColors(clBlack,clWhite);
   /////// Creación de Métodos: ///////
   oneEqBisection := TBisectionMethod.create();
   oneEqFalsePosition := TFalsePositionMethod.create();
   oneEqNewton := TNewtonMethod.create();
   oneEqSecant := TSecantMethod.create();
   oneEqFixPoint := TFixPointMethod.create();

   polLagrange := TLagrangePol.create();
   polNewton := TNewtonPol.create();

   genNewton := TNewtonGenMethod.create();

   odeRungeK := TRungeKuttaMethod.create();
   odeHeun := THeunMethod.create();
   odeEuler := TEulerMethod.create();
   odeDormandP := TDormandPrinceMethod.create();

   itgSimpson1_3 := TSimpson1_3Method.create();
   itgSimpson3_8 := TSimpson3_8Method.create();
   itgTrapezoidal := TTrapezoidalMethod.create();

   areaCalc := TAreaMethod.create();

   ans:='';
   ///////////////////////////////////

   self.StartCommand();
end;

procedure TmainForm.cmdBoxInput(aCmdBox: TCmdBox; input: string);
var
   parsedList, auxList : TStringList;
   narg1, narg2, narg3, narg4,narg5, narg6 : real;
   odeRes, historicPts : TArrxy;
   initialPoint : TMatrix;
   vars : TArrStr;
   itgRes, rootRes : real;
   polRes : string;
   i, numEq : integer;
   eqSys : string;
   min, max, harea, auxArea : real;
   auxCnt : integer;
begin
   input:= trim(input);
   parsedList := cmdParser.parseString(input);
   // FORMATO: [method | operation], [plot | noplot],[methodName*], function, arg1, ..., argN
   // * solo si es método

   if parsedList[0] = 'method' then
   begin
      // no olvidarse de poner los mismos nombres en el cmdParser!!!!!!!!
      cmdBox.writeln(parsedList.CommaText);

      case parsedList[2] of
         'bisection' :
            begin
               narg1 := myStrToFloat(parsedList[4]);
               narg2 := myStrToFloat(parsedList[5]);
               oneEqBisection.setData(parsedList[3],narg1,narg2);
               rootRes := oneEqBisection.solve();
               cmdBox.Writeln(formatFloat(formatF, rootRes));

               if parsedList[1] = 'plot' then
                  begin
                  frmGraph.setMethod('root');
                  frmGraph.setDataRoot(parsedList[3],rootRes);
                  frmGraph.show;
                  end;
            end;
         'falsePosition' :
            begin
               narg1 := myStrToFloat(parsedList[4]);
               narg2 := myStrToFloat(parsedList[5]);
               oneEqFalsePosition.setData(parsedList[3],narg1,narg2);
               rootRes :=  oneEqFalsePosition.solve();
               cmdBox.Writeln(formatFloat(formatF, rootRes));

               if parsedList[1] = 'plot' then
                  begin
                  frmGraph.setMethod('root');
                  frmGraph.setDataRoot(parsedList[3],rootRes);
                  frmGraph.show;
                  end;
            end;
         'newton' :
            begin
               narg1 := strToFloat(parsedList[4]);
               oneEqNewton.setData(parsedList[3],narg1);
               rootRes :=  oneEqNewton.solve();
               cmdBox.Writeln(formatFloat(formatF, rootRes));

               if parsedList[1] = 'plot' then
                  begin
                  frmGraph.setMethod('root');
                  frmGraph.setDataRoot(parsedList[3],rootRes);
                  frmGraph.show;
                  end;
            end;
         'secant' :
            begin
               narg1 := strToFloat(parsedList[4]);
               oneEqSecant.setData(parsedList[3],narg1);
               rootRes :=  oneEqSecant.solve();
               cmdBox.Writeln(formatFloat(formatF, rootRes));

               if parsedList[1] = 'plot' then
                  begin
                  frmGraph.setMethod('root');
                  frmGraph.setDataRoot(parsedList[3],rootRes);
                  frmGraph.show;
                  end;
            end;
         'fixPoint' :
            begin
               narg1 := strToFloat(parsedList[6]);
               oneEqFixPoint.setData(parsedList[3],parsedList[4],parsedList[5],narg1);
               rootRes :=  oneEqFixPoint.solve();
               cmdBox.Writeln(formatFloat(formatF, rootRes));

               if parsedList[1] = 'plot' then
                  begin
                  frmGraph.setMethod('root');
                  frmGraph.setDataRoot(parsedList[3],rootRes);
                  frmGraph.show;
                  end;
            end;
         'lagrangePol' :
            begin
               setLength(historicPts, 2, parsedList.count - 4); // 2 x N (x,y)
               auxList := TStringList.create();
               for i := 4 to  parsedList.count - 1 do
               begin
                  auxList.clear();
                  extractStrings([' '],[],PChar(trim(parsedList[i])), auxList);
                  historicPts[0][i - 4] := strToFloat( auxList[0]);
                  historicPts[1][i - 4] := strToFloat(auxList[1]);
               end;
               polLagrange.setData(historicPts);
               cmdBox.Writeln(matrixToStr(historicPts));
               polRes := polLagrange.solve();
               cmdBox.Writeln(polRes);

               if parsedList[1] = 'plot' then
               begin
                  frmGraph.setMethod('polinomial');
                  frmGraph.setDataPol(historicPts[0][0], historicPts[0][length(historicPts[0])- 1],
                                     polRes);
                  frmGraph.show;
               end;
               //cmdBox.Writeln(formatFloat(formatF, lagrangePolObj.eval(1.5)))
            end;
         'newtonPol' :
            begin
               setLength(historicPts, 2, parsedList.count - 4); // 2 x N (x,y)
               auxList := TStringList.create();
               for i := 4 to  parsedList.count - 1 do
               begin
                  auxList.clear();
                  extractStrings([' '],[],PChar(trim(parsedList[i])), auxList);
                  historicPts[0][i - 4] := strToFloat( auxList[0]);
                  historicPts[1][i - 4] := strToFloat(auxList[1]);
               end;
               polNewton.setData(historicPts);
               cmdBox.Writeln(matrixToStr(historicPts));
               polRes := polNewton.solve();
               cmdBox.Writeln(polRes);
               ans:=polRes;               //guardar anterior

               if parsedList[1] = 'plot' then
               begin
                  frmGraph.setMethod('polinomial');
                  frmGraph.setDataPol(historicPts[0][0], historicPts[0][length(historicPts[0])- 1],
                                      polRes);
                  frmGraph.show;
               end;
               //cmdBox.Writeln(formatFloat(formatF, lagrangePolObj.eval(1.5)))

            end;
         'newtonGeneralizado' :
            begin
               eqSys := parsedList[3];
               numEq := strToInt(parsedList[parsedList.count - 1]);

               setLength(vars, numEq);
               setLength(initialPoint, numEq, 1); // N x 2

               for i:= 0 to numEq - 1 do
               begin
                  vars[i] := trim(parsedList[4 + numEq + i]);
                  initialPoint[i][0] := strToFloat(parsedList[4 + i]);
               end;
               genNewton.setData(eqSys, initialPoint, vars); // ({[ada;sd]},1,1,1, x,y,z, 3)
               cmdBox.writeln(matrixToStr(genNewton.solve()));
            end;
         'odeRungeK' :
            begin
               narg1 := myStrToFloat(parsedList[4]);
               narg2 := myStrToFloat(parsedList[5]);
               narg3 := myStrToFloat(parsedList[6]);
               odeRungeK.setData(parsedList[3], narg1, narg2, narg3);
               odeRes := odeRungeK.solve();
               cmdBox.writeln(self.matrixToStr(odeRes));

               if parsedList[1] = 'plot' then
               begin
                  frmGraph.setMethod('ode');
                  frmGraph.setDataOde(odeRes);
                  frmGraph.show;
               end;
            end;
         'odeHeun':
            begin
               narg1 := myStrToFloat(parsedList[4]);
               narg2 := myStrToFloat(parsedList[5]);
               narg3 := myStrToFloat(parsedList[6]);
               odeHeun.setData(parsedList[3], narg1, narg2, narg3);
               odeRes := odeHeun.solve();
               cmdBox.writeln(self.matrixToStr(odeRes));

               if parsedList[1] = 'plot' then
               begin
                  frmGraph.setMethod('ode');
                  frmGraph.setDataOde(odeRes);
                  frmGraph.show;
               end;

            end;
         'odeEuler':
            begin
               narg1 := myStrToFloat(parsedList[4]);
               narg2 := myStrToFloat(parsedList[5]);
               narg3 := myStrToFloat(parsedList[6]);
               odeEuler.setData(parsedList[3], narg1, narg2, narg3);
               odeRes := odeEuler.solve();
               cmdBox.writeln(self.matrixToStr(odeRes));

               if parsedList[1] = 'plot' then
               begin
                  frmGraph.setMethod('ode');
                  frmGraph.setDataOde(odeRes);
                  frmGraph.show;
               end;

            end;
         'odeDormandP':
            begin
               narg1 := myStrToFloat(parsedList[4]);
               narg2 := myStrToFloat(parsedList[5]);
               narg3 := myStrToFloat(parsedList[6]);
               odeDormandP.setData(parsedList[3], narg1, narg2, narg3);
               odeRes := odeDormandP.solve();
               cmdBox.writeln(self.matrixToStr(odeRes));

               if parsedList[1] = 'plot' then
               begin
                  frmGraph.setMethod('ode');
                  frmGraph.setDataOde(odeRes);
                  frmGraph.show;
               end;
            end;
         'itgSimpson1_3':
            begin
               narg1 := myStrToFloat(parsedList[4]);
               narg2 := myStrToFloat(parsedList[5]);
               if parsedList[3] = 'Ans' then
                  parsedList[3] := ans;
               itgSimpson1_3.setData(parsedList[3], narg1, narg2);
               itgRes := itgSimpson1_3.solve();
               cmdBox.writeln(formatFloat(formatF, itgRes));

               if parsedList[1] = 'plot' then
               begin
                  frmGraph.setMethod('integral');
                  frmGraph.setDataIntegral(narg1, narg2, parsedList[3]);
                  frmGraph.show;
               end;
            end;
         'itgSimpson3_8':
            begin
               narg1 := myStrToFloat(parsedList[4]);
               narg2 := myStrToFloat(parsedList[5]);

               if parsedList[3] = 'Ans' then
                  parsedList[3] := ans;

               itgSimpson3_8.setData(parsedList[3], narg1, narg2);

               itgRes := itgSimpson3_8.solve();
               cmdBox.writeln(formatFloat(formatF, itgRes));
               ans := formatFloat(formatF, rootRes);
               if parsedList[1] = 'plot' then
               begin
                  frmGraph.setMethod('integral');
                  frmGraph.setDataIntegral(narg1, narg2, parsedList[3]);
                  frmGraph.show;
               end;
            end;

         'area':
            begin
               narg1 := myStrToFloat(parsedList[4]);
               narg2 := myStrToFloat(parsedList[5]);

               if parsedList[3] = 'Ans' then
                  parsedList[3] := ans;

               areaCalc.setData(parsedList[3], narg1, narg2);

               itgRes := areaCalc.solve();
               cmdBox.writeln(formatFloat(formatF, itgRes));
               ans := formatFloat(formatF, rootRes);
               if parsedList[1] = 'plot' then
               begin
                  frmGraph.setMethod('integral');
                  frmGraph.setDataIntegral(narg1, narg2, parsedList[3]);
                  frmGraph.show;
               end;
            end;
         'areasFromTo':
            begin
               min := myStrToFloat(parsedList[4]);
               auxArea := min;
               max := myStrToFloat(parsedList[5]);
               harea := 0.01;
               auxCnt := 0;
               while (auxArea <= max) do
               begin
                  itgSimpson3_8.setData(parsedList[3], min, auxArea);
                  itgRes := itgSimpson3_8.solve();
                  if auxCnt = 10 then
                  begin
                     auxCnt := 0;
                     cmdBox.write(sLineBreak);
                  end;
                  cmdBox.write(formatFloat(formatF, itgRes) + ' ');
                  auxCnt := auxCnt + 1;
                  auxArea := auxArea + harea;
               end;

               if parsedList[1] = 'plot' then
               begin
                  frmGraph.setMethod('integral');
                  frmGraph.setDataIntegral(min, max, parsedList[3]);
                  frmGraph.show;
               end;
            end;

         'itgTrapezoidal':
            begin
               narg1 := myStrToFloat(parsedList[4]);
               narg2 := myStrToFloat(parsedList[5]);

               if parsedList[3] = 'Ans' then
                  parsedList[3] := ans;
               itgTrapezoidal.setData(parsedList[3], narg1, narg2);

               itgRes := itgTrapezoidal.solve();
               cmdBox.writeln(formatFloat(formatF, itgRes));

               if parsedList[1] = 'plot' then
               begin
                  frmGraph.setMethod('integral');
                  frmGraph.setDataIntegral(narg1, narg2, parsedList[3]);
                  frmGraph.show;
               end;
            end;
         'help':
            begin
               cmdBox.writeln('Solve One equation methods');
               cmdBox.writeln('      bisection({ecuacion},a,b)');
               cmdBox.writeln('      falsePosition({ecuacion},a,b)');
               cmdBox.writeln('      newton({ecuacion},x0)');
               cmdBox.writeln('      secant({ecuacion},x0)');
               cmdBox.writeln('      fixPoint({ecuacion}, df/dx(x), g(x), x0)');
            end;
         'Ans':
            begin
            cmdBox.Writeln('ans. '+ ans);
            end;
         // TODO: crear variable para guardar respuestas,
      end;
   end
   else if parsedList[0] = 'operation' then
   begin
      cmdBox.writeln(self.matrixToStr(mathParser.solveExpression(parsedList[2])));
      if parsedList[1] = 'plot' then
         showMessage('PLOT'); // TODO: hacer el ploteo;
   end
   else //ERROR
      exit;
   self.startCommand();
end;

procedure TmainForm.btnSaveHistoryClick(Sender: TObject);
begin
   cmdBox.SaveToFile('minilab.sav');
   ShowMessage('Archivo Guardado.');
end;

procedure TmainForm.FormDestroy(Sender: TObject);
begin
   mathParser.destroy();
   cmdParser.destroy();
end;

procedure TmainForm.btnAddConstClick(Sender: TObject);
begin
   frmAddConstant.setParser(@mathParser);
   frmAddConstant.show();
end;

function TmainForm.matrixToStr(constref input : TMatrix) : string;
var
   i, j : integer;
   res :string;
   cols, rows : integer;
begin
   if (isSingle(input)) then
      Result := formatFloat(formatF, input[0][0])
   else
   begin
      rows := countRows(input);
      cols := countCols(input);
      res := sLineBreak;
      for i := 0 to rows - 1 do
      begin
         for j := 0 to cols - 1 do
         begin
            res += formatFloat(formatF, input[i][j]) + '  ';
         end;
         res += sLineBreak;
      end;
      Result:= res;
   end;
end;

procedure TmainForm.StartCommand();
begin
   cmdBox.startRead( clBlue, clWhite,  '> ', clBlack, clWhite);
end;

function TmainForm.myStrToFloat(str: string) : real;
var
   auxReal : real;
begin
   if tryStrToFloat(str, auxReal) then
      exit(auxReal)
   else
      exit(self.mathParser.solveExpression(str)[0][0]);
end;

end.
