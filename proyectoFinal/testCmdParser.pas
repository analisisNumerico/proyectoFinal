program find;

{$mode objfpc}{$H+}

uses
   math, strutils,    Classes, SysUtils, CmdParser;

var
   str : string;
   list : TStringList;
   fnd : integer;
   parser: TCmdParser;
begin
   parser := TCmdParser.create();
   list := TStringList.create();

   //str := 'method1({1+1*algo(1,2)} , 1 , 2 , 3 , 4        )     &            plot ';
   //str := 'method1([1,2;3,4] , algo(1,2), 1 + 32 , pepito, (1+2)*(3-(2-3)), ((2*pi)^(1/2))*exp(-(x^2/2))) & plot';
   str := 'odeRungeK((-x)/((2*pi)^(1/2))*exp(-(x^2/2)),-4,0, 4) & plot';
   list :=parser.parseString(str);

   // write('---');
   writeln(list.commatext);
   // write('---');
    writeln(list.count);
end.
