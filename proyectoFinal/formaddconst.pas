unit formAddConst;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, mCalculator;

type
   
   { TfrmAddConstant }
   TfrmAddConstant = class(TForm)
      btnSave: TButton;
      edtIn: TEdit;
      procedure btnSaveClick(Sender: TObject);
   private
      parser : ^TCalculator;
      { private declarations }
   public
      procedure setParser(ptr : PtrCalculator);
      { public declarations }
   end;

var
   frmAddConstant: TfrmAddConstant;

implementation

{$R *.lfm}

{ TfrmAddConstant }

procedure TfrmAddConstant.btnSaveClick(Sender: TObject);
var
   list : TStringList;
begin
   if (edtIn.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese el nombre seguido de su valor.');
      exit;
   end;
   list := TStringList.create();
   extractStrings(['='],[],PChar(self.edtIn.text), list);
   parser^.addConst(trim(list[0]), strToFloat(list[1]));
   self.close();
   self.edtIn.Text := '';
   ShowMessage('Constante Guardada.');
end;

procedure TfrmAddConstant.setParser(ptr: PtrCalculator);
begin
   self.parser := ptr;
end;

end.

