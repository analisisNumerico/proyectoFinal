program minilab;

{$mode objfpc}{$H+}

uses
   {$IFDEF UNIX}{$IFDEF UseCThreads}
   cthreads,
   {$ENDIF}{$ENDIF}
   Interfaces, // this includes the LCL widgetset
   Forms, tachartlazaruspkg, cmdbox, mainwindow, formaddconst, formgraph, newtongeneralizadomethod1
   { you can add units after this };

{$R *.res}

begin
   RequireDerivedFormResource:=True;
   Application.Initialize;  
   Application.CreateForm(TmainForm, mainForm);
   Application.CreateForm(TfrmAddConstant, frmAddConstant);
   Application.CreateForm(TfrmGraph, frmGraph);
   Application.Run;
end.


