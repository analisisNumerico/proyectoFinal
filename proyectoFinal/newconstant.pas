unit NewConstant;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, mCalculator, Dialogs;

type
   { TfrmNewConstant }
   TfrmNewConstant = class(TForm)
      btnSET: TButton;
      edNewChar: TEdit;
      edNewVal: TEdit;
   private
      { private declarations }
      calculator : TCalculator;
   public
      { public declarations }

   end;

implementation

procedure TfrmNewConstant.FormCreate(Sender: TObject);
begin
   calculator := TCalculator.create();
   calculator.solveExpression('x = 0');
end;

procedure TfrmNewConstant.FormDestroy(Sender: TObject);
begin
   calculator.destroy();
end;

procedure TfrmNewConstant.FormShow(Sender: TObject);
begin
   ShowMessage('Inserte su constante en los espacios');
end;
end.

