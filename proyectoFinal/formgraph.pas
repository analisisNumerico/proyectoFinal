unit formGraph;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, FileUtil, TAGraph, TASeries, TAFuncSeries, Forms, Controls,
   Graphics, Dialogs, ExtCtrls, StdCtrls, ColorBox, TAChartUtils,
   TASources, TATools  ,TACustomSeries, mCalculator;

type

   { TfrmGraph }
   Treal22 = array of array of real;
   TfrmGraph = class(TForm)
      chrGrafica: TChart;
      areaSeries: TAreaSeries;
      pointSeries: TLineSeries;
      plotSeries: TLineSeries;
      axisX: TConstantLine;
      axisY: TConstantLine;
      procedure areaSeriesGetMark(out AFormattedMark: String; AIndex: Integer);
      procedure FormCreate(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
      procedure FormShow(Sender: TObject);

   private
      { private declarations }
      calculator : TCalculator;

      _areaLow : real;
      _areaHigh : real;
      _funcLow : real;
      _funcHigh : real;
      _xyVals : Treal22;
      _method : string;
      _root : real;
      _minh : real;
      _maxh : real;
      function f(x: Double): Double;

   public
      { public declarations }
      procedure setDataIntegral(low, high : real; funct : string);
      procedure setDataOde(xyVals : Treal22);  //Array x-y
      procedure setDataRoot(funct: string; x : real);
      procedure setDataPol(minh, maxh : real;funct: string);
      procedure setMethod(method : string);

      procedure graphFunctIntegral();
      procedure graphFunctOde();
      procedure graphFunctRoot();
      procedure graphFunctPol();
   end;

var
   frmGraph: TfrmGraph;


implementation

{$R *.lfm}

{ TfrmGraph }

procedure TfrmGraph.setDataIntegral(low, high: real; funct: string);
var
   tmp : real;
begin
   tmp := (high - low) / 6;
   _funcLow := low - tmp  /2;
   _funcHigh := high + tmp /2;
   _areaLow := low;
   _areaHigh := high;
   calculator.solveExpression(funct);
end;

procedure TfrmGraph.setDataOde(xyVals : Treal22);
begin
   _xyVals := xyVals;
end;

procedure TfrmGraph.setDataRoot(funct: string; x : real);
begin
   calculator.solveExpression(funct);
   _root := x;
end;

procedure TfrmGraph.setDataPol(minh, maxh : real; funct: string);
begin
   _minh := minh;
   _maxh := maxh;
   calculator.solveExpression(funct);
end;


function TfrmGraph.f(x: Double): Double;
begin
   Result := calculator.solveSavedExpression(['x'], [x])[0][0];
end;



// Para las funciones de integrales
procedure TfrmGraph.graphFunctIntegral();
var
   i: Integer;
   max, min, h, newX, newY: real;
begin
   areaSeries.active:= false;
   plotSeries.active:= false;
   max :=  _funcHigh;
   min :=  _funcLow;


   h:= abs((max - min)/(100 * max));

   NewX:= _funcLow;

   while newX < max do begin
      newY := f(newX);
      plotSeries.AddXY(newX, newY);
      if (newX >= _areaLow) and (newX <= _areaHigh) then
         areaSeries.addXY(newX, newY);
      newX:= newX + h;
   end;

   plotSeries.active:= true;
   areaSeries.active:= true;
end;

procedure TfrmGraph.graphFunctOde();
var
   i: integer;
begin
   plotSeries.active:= false;

   for i := 0 to length(_xyVals) - 1 do
   begin
      plotSeries.AddXY(_xyVals[i][0], _xyVals[i][1]);
   end;

   plotSeries.active:= true;
end;

procedure TfrmGraph.graphFunctRoot();
var
   i: integer;
   max, min, h, newX, newY: real;
begin
   plotSeries.active := false;
   pointSeries.active := false;
   max :=  _root + 5; //Todo: o tiene que ser del intervalo que ingreso el user
   min :=  _root - 5; //Todo: o tiene que ser del intervalo que ingreso el user
   h:= abs((max - min)/(100 * max));
   newX:=min;
   while newX < max do
      begin
      newY := f(newX);
      plotSeries.AddXY(newX, newY);
      newX:= newX + h;
      end;

   pointSeries.AddXY(_root, 0); // Para asignar el punto
   plotSeries.active := true;
   pointSeries.active := true;
end;

procedure TfrmGraph.graphFunctPol();
var
   i: integer;
   max, min, h, newX, newY: real;
begin
   plotSeries.active := false;
   max :=  _maxh;
   min :=  _minh;
   h:= abs((max - min)/(100 * max));
   while newX < max do
      begin
      newY := f(newX);
      plotSeries.AddXY(newX, newY);
      newX:= newX + h;
      end;

   plotSeries.active := true;
end;

// Es necesario setear un método: 'ode', 'integral', 'root', ...
procedure TfrmGraph.setMethod(method : string);
begin
   self._method := method;
end;

procedure TfrmGraph.areaSeriesGetMark(out AFormattedMark: String; AIndex: Integer);
begin
end;

procedure TfrmGraph.FormCreate(Sender: TObject);
begin
   calculator := TCalculator.create();
   calculator.solveExpression('x = 0');
end;

procedure TfrmGraph.FormDestroy(Sender: TObject);
begin
   calculator.destroy();
end;

// Dependiendo de qué método se haya seteado, grafica una u otra cosa:
procedure TfrmGraph.FormShow(Sender: TObject);
begin
   areaSeries.clear;
   plotSeries.clear;
   pointSeries.clear;
   // Para asegurar que no se dibujen si no quiero :
   areaSeries.active:= false;
   plotSeries.active:= false;
   pointSeries.active:= false;

   if self._method = 'integral' then
      self.graphFunctIntegral()
   else if self._method = 'ode' then
      self.graphFunctOde()
   else if self._method = 'root' then
      self.graphFunctRoot()
   else if self._method = 'polinomial' then
      self.graphFunctPol()

   // ... TODO: las que faltan
end;


end.
