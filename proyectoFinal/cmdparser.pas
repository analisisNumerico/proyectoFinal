unit CmdParser;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, strutils;

type

   TCmdParser = class
   private
      methodsNames : TStringList;
      function extractOperation(constref str: string) : string;
      function extractArgs(constref str: string; i : integer) : TStringList;
      function hasplot(constref str: string) : boolean;
      procedure subCommas(constref str: string; var i : integer);

   public
      constructor create();
      destructor destroy(); override;
      // FORMATO: [method | operation], [plot | noplot], function, arg1, ..., argN
      function parseString(constref str : string) : TStringList;
   end;

implementation

constructor TCmdParser.create();
begin
   methodsNames := TStringList.create();
   methodsNames.sorted := true;
   methodsNames.addStrings(['method1','help','Ans',
                            'bisection','falsePosition',
                            'newton','secant','fixPoint',
                            'lagrangePol', 'newtonPol',
                            'newtonGeneralizado',
                            'odeRungeK', 'odeHeun', 'odeEuler' , 'odeDormandP',
                            'itgSimpson1_3' , 'itgSimpson3_8', 'itgTrapezoidal',
                            'areasFromTo', 'area']);
end;

destructor TCmdParser.destroy();
begin
   methodsNames.destroy();
end;

{
 Notas:
 - copy(str, a, b) crea un substring desde a, con b caracteres;
 - pos(fnd, str) busca fnd en str, retorna la posición de fnd si lo encuentra, sino retora 0
 - xd
}
function TCmdParser.parseString(constref str : string) : TStringList;
var
   fnd, tmp : integer;
   substr : string;
   res : TStringList;
begin
   res := TStringList.create();

   // Busco un paréntesis en el string ingresado:
   fnd := pos('(', str);
   substr := copy(str, 1, fnd - 1);
   substr := trim(substr);

   // Si el substr es el nombre de un método:
   if (length(substr) <> 0) and  methodsNames.find(substr, tmp) then // Si el substr es el nombre de un método
   begin
      res.add('method');
      if (self.hasplot(str)) then
         res.add('plot')
      else
         res.add('noplot');
      res.add(substr);
      res.addStrings(self.extractArgs(str, fnd + 1));
   end
   else // Si no es método, entonces es una operación
   begin
      res.add('operation');
      if (self.hasplot(str)) then
         res.add('plot')
      else
         res.add('noplot');
      res.add(self.extractOperation(str));
   end;
   Result := res;
end;

function TCmdParser.extractOperation(constref str: string) : string;
var
   fnd : integer;
begin
   fnd := pos('&', str);
   if fnd = 0 then
      Result := trim(str)
   else
      Result := trim(copy(str,1, fnd - 1))
end;

function TCmdParser.extractArgs(constref str: string; i : integer) : TStringList;
var
   aux : string;
   first, last : integer;
   auxStrList : TStringList;
begin
   auxStrList := TStringList.create();
   last := rpos(')',str);
   first := i;
   while i < last do
   begin
      if str[i] = ',' then
      begin
         aux := copy(str, first, i - first);
         auxStrList.add(trim(aux));
         i := i + 1;
         first := i;
      end
      else if str[i] in ['(', '['] then
         self.subCommas(str, i)
      else
         i := i + 1;
   end;
   aux := copy(str, first, i - first);
   auxStrList.add(trim(aux));
   Result := auxStrList;
end;

// Lee las comas internas de funciones matrices
procedure TCmdParser.subCommas(constref str: string; var i : integer);
begin
   i := i + 1;
   while not (str[i] in [')', ']']) do
   begin
      if str[i] in ['(', '['] then
         self.subCommas(str, i)
      else
         i := i + 1;

   end;
   i := i + 1;
end;

function TCmdParser.hasplot(constref str: string) : boolean;
var
   fnd: integer;
begin
   fnd := pos('&', str);
   if fnd <> 0 then
      Result := (trim(copy(str, fnd + 1, length(str))) = 'plot')
   else
      Result := false;
end;

end.
