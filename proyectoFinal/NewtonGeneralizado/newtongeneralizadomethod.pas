unit NewtonGeneralizadoMethod;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, mCalculator, mMatrix;
type
   TNewtonGeneralizado = class
   protected
      _function : TCalculator;
      _x0 : TMatrix;
      _sistema : array of string;
      _numEqu : integer;
      _error : real;
   public
      constructor create();
      destructor destroy(); override;

      procedure setData(x0: TMatrix; sistema: array of string; numEq: integer);
      function solve() : TMatrix;

      function evaluate (ecuacion : string; variables : TMatrix): real;
      function DerivadaParcial(ecuacion: string; i : Integer; variables: TMatrix):real;
      Function Jacobiana(sistema: array of string; variables: TMatrix):TMatrix;
   end;


implementation

constructor TNewtonGeneralizado.create();
begin
   _function := TCalculator.create();
end;

destructor TNewtonGeneralizado.destroy();
begin
   _function.destroy();
end;

procedure TNewtonGeneralizado.setData(x0: TMatrix; sistema: array of string; numEq: integer);
begin
   _x0 := x0;
   _sistema := sistema;
   _numEqu := numEq ;
   _error := 0.01;
end;

function TNewtonGeneralizado.evaluate (ecuacion : string;variables : TMatrix): real;
var
  n, i : integer;
  letra : char;
begin
     n:=Length(variables);
     letra:='x';
     for i:=0 to n-1 do
      begin
           _function.AddVariable( letra,variables[i,0]);
           letra:=succ(letra);
           if letra>'z' then
            letra:='a';
      end;
     Result := (_function.solveExpression(ecuacion))[0][0];
end;

function TNewtonGeneralizado.DerivadaParcial(ecuacion : string; i : Integer; variables : TMatrix):Real;
var
    fxh, fx, h:Real;
begin
     h:=0.0000001;

     fx:=evaluate(ecuacion,variables);
     variables[i][0]:=variables[i][0]+h;

     fxh:=evaluate(ecuacion,variables);
     Result:=(fxh-fx)/h;
end;

function TNewtonGeneralizado.Jacobiana(sistema: array of String; variables: TMatrix):TMatrix;
var
  x,y,n : integer;
  tmp : TMatrix;
begin
     n:= _numEq;
     SetLength(tmp,n,n);
     for x:=0 to n-1 do
        for y:=0 to n-1 do
                 tmp[x,y]:=DerivadaParcial(sistema[x],y,variables);
     Result:= tmp;
end;

function TNewtonGeneralizado.solve():TMatrix;
var
  puntoI0, puntoI1, jacobi,inversa,fx : TMatrix;
  i, n : integer;
  Ea : real;
  errorDistance : array of real;
begin
  SetLength(_sistema,_numEqu);
  SetLength(errorDistance, _numEqu);
  SetLength(fx,numEq,1);
  Ea:= 100;
  n:=0;
  puntoI0 := _x0;
  while (Ea>_error) do
        begin
        for i:= 0 to _numEq-1 do
           begin
           fx[i][0]:=evaluate(_sistema[i],puntoI0);
           end;
        jacobi:= Jacobiana(_sistema,puntoI0);
        inversa := finverse(jacobi);
        {x0-inversa(jacobiana(xo))*funcion(x0)}
        puntoI1:= fsub([puntoI0, fmul([inversa, fx])]);
        for i:=0 to numEq-1 do
           begin
           errorDistance[i] := fpow([fsub([ createMatrix(puntoI0[i][0]),createMatrix(puntoI1[i][0])]),createMatrix(2)])[0][0];
           end;
        Ea:= fsqrt([fsum(createMatrix(1,_numEqu,errorDistance))])[0][0];
        puntoI0:= puntoI1;
        n:=n+1;
        end;
  Result := puntoI1;
end;

end.

