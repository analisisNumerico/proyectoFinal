unit DormandPMethod;

{$mode objfpc}{$H+}

interface

uses
   math, mCalculator, mFuncImp, mMatrix, BaseMethod;

type
   TDormandPrinceMethod = class(TBaseMethod)
   public
      function solve() : TArrxy; override;
   end;

implementation

function TDormandPrinceMethod.solve() : TArrxy;
var
   xyValues : TArrxy;
   k1, k2, k3, k4, k5, k6 ,k7 : real;
   xi, yi, ytmp, ztmp : real;
   i : integer;
   s, eps : real; // s y epsilon
begin
   eps := 0.0001;
   _h := 0.1;
   setLength(xyValues, 1, 2); // N x 2;
   xyValues[0][0] := _x0;
   xyValues[0][1] := _y0;
   i := 0;
   while (xyValues[0][i] < _xf) do
   begin
      xi := xyValues[i][0];
      yi := xyValues[i][1];

      k1 := _h * _function.solveSavedExpression(['x', 'y'],[xi, yi])[0][0];
      k2 := _h * _function.solveSavedExpression(
         ['x', 'y'],[xi + 1/5*_h ,
                     yi + 1/5*k1])[0][0];
      k3 := _h * _function.solveSavedExpression(
         ['x', 'y'],[xi + 3/10*_h ,
                     yi + 3/40*k1 + 9/40*k2])[0][0];
      k4 := _h * _function.solveSavedExpression(
         ['x', 'y'],[xi + 4/5*_h ,
                     yi + 44/45*k1 - 56/15*k2 + 32/9*k3])[0][0];
      k5 := _h * _function.solveSavedExpression(
         ['x', 'y'],[xi + 8/9*_h ,
                     yi + 19372/6561*k1 - 25360/2187*k2
                     + 64448/6561*k3 - 212/729*k4])[0][0];
      k6 := _h * _function.solveSavedExpression(
         ['x', 'y'],[xi + _h ,
                     yi + 9017/3168*k1 - 355/33*k2 + 46732/5247*k3
                     + 49/172*k4 - 5103/18656*k5])[0][0];
      k7 := _h * _function.solveSavedExpression(
         ['x', 'y'],[xi + _h ,
                     yi + 35/384*k1 + 500/1113*k3 + 125/192*k4
                     - 2187/6784*k5 + 11/84*k6])[0][0];

      ytmp :=
         yi + 35/384*k1 + 500/1113*k3 + 125/192*k4 - 2187/6784*k5 + 11/84*k6;
      ztmp := yi + 5179/57600*k1 + 7571/16695*k3 + 393/640*k4 - 92097/339200*k5
         + 187/2100*k6 + 1/40*k7;

      // para calcular s :
      s := power(_h*eps/(2*(_xf - _x0)*abs(ytmp - ztmp)), 1/4);
      if s >= 2 then
      begin
         setLength(xyValues, length(xyValues) + 1, 2);
         xyValues[0][i + 1] := xi + _h;
         xyValues[1][i + 1] := ytmp;
         i := i + 1;
         _h := _h * 2;
      end
      else if s >= 1 then
      begin
         setLength(xyValues,length(xyValues) + 1, 2);
         xyValues[i + 1][0] := xi + _h;
         xyValues[i + 1][1] := ytmp;
         i := i + 1;
      end
      else
         _h := _h / 2;

      if (xyValues[i][0] + _h > _xf) then
         _h := _xf - xyValues[i][0];
   end;
   Result := xyValues;
end;

end.
