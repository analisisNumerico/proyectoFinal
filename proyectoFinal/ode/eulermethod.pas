unit EulerMethod;

{$mode objfpc}{$H+}

interface

uses
   math, mCalculator, mFuncImp, mMatrix, BaseMethod;

type
   TEulerMethod = class(TBaseMethod)
   public
      function solve() : TArrxy; override;
   end;

implementation
function TEulerMethod.solve() : TArrxy;
var
   xyValues : TArrxy;
   i : integer;
   xi, yi : real;
begin
   setLength(xyValues, 1, 2); // N x 2;
   xyValues[0][0] := _x0;
   xyValues[0][1] := _y0;
   i := 0;
   while (xyValues[i][0] <= _xf) do
   begin
      xi := xyValues[i][0];
      yi := xyValues[i][1];
      setLength(xyValues, length(xyValues) + 1, 2);
      xyValues[i + 1][1] := yi + _h * _function.solveSavedExpression(['x', 'y'],[xi, yi])[0][0];
      xyValues[i + 1][0] := xi + _h;

      i := i + 1;
   end;

   Result := xyValues;

end;

end.
