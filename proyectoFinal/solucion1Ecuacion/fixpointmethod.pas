unit FixPointMethod;

{$mode objfpc}{$H+}

interface

uses
   math, mCalculator, Dialogs;

type
   TFixPointMethod = class
   protected
      _function : TCalculator;
      _x0 : real;
      _error : real;
      _fdx : string;
      _gx : string;
   public
      constructor create();
      destructor destroy(); override;

      procedure setData(func, fdx, gx : string; x0: real);
      function solve() : real;
      function f(x: real; auxCalculator: Tcalculator) : real;
      function continuo(x: real; auxCalculator: Tcalculator) : boolean;
      function calNextX(xn : real) : real;
      function checkConvergence() : boolean;
   end;

implementation

constructor TFixPointMethod.create();
begin
   _function := TCalculator.create();
end;

destructor TFixPointMethod.destroy();
begin
   _function.destroy();
end;
procedure TFixPointMethod.setData(func, fdx, gx : string; x0: real);
begin
   _function.solveExpression(func);
   _x0 := x0;
   _fdx := fdx;
   _gx := gx ;
   _error := 0.001;
end;

function TFixPointMethod.f(x: real; auxCalculator: Tcalculator):real;
begin
  Result:=(auxCalculator.solveSavedExpression(['x'],[x])).[0][0];
end;


function TFixPointMethod.calNextX(xn :real) : real;
begin
    _function.solveExpression(_gx);
    Result := f(xn,_function);
end;


function TFixPointMethod.checkConvergence() : boolean;
var
  gdn :real;
begin
  gdn:= _function.solveExpression(_fdx)[0][0];
  if (gdn > 1) OR (gdn < 0) then Result := false else Result:= true;
end;

function TFixPointMethod.continuo(x: real; auxCalculator: Tcalculator):Boolean;
var t : real;
begin
     Try
       t:= f(x,auxCalculator);
       Result := True;
     Except
       Result:=False;
     end;
end;

function TFixPointMethod.solve() : real;
var   Ea, xn, xnPrev, n : real;
begin
  xn := _x0;
  n := 0;
  Ea:=100;
  if continuo(_x0,_function) then
   begin
      while (Ea > _error) AND continuo(xn, _function) AND (n < 20) do
         begin
            if f(xn,_function) = 0 then exit(xn);
            if checkConvergence() then
               begin
                  ShowMessage('La serie probablemente diverge');
                  exit(nan);
               end;
            xnPrev := xn;
            xn := calNextX(xn);
            if (n > 0) then Ea:= xn - xnPrev;
            n:=n+1 ;
         end;
      exit(xn);
   end
  else
     exit(nan);
end;


end.
