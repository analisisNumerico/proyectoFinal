unit SolveOpenEquationBaseMethod;

{$mode objfpc}{$H+}

interface

uses
    mCalculator, Dialogs, math, Classes, SysUtils;

type
   TSolveOpenEquationBaseMethod = class
   protected
      _function : TCalculator;
      _x0 : real;
      _error : real;
   public
      constructor create();
      destructor destroy(); override;

      procedure setData(func : string; x0: real);
      function solve() : real;

      function calNextX(x :real) : real; virtual; abstract;
      function check0Derivative(x: real) : boolean; virtual; abstract;
      function f(x: real; auxCalculator: Tcalculator) : real;
      function continuo(x: real; auxCalculator: Tcalculator):boolean;
      function fDerivative(x : real; cal :TCalculator) : real;
   end;

implementation

constructor TSolveOpenEquationBaseMethod.create();
begin
   _function := TCalculator.create();
end;

destructor TSolveOpenEquationBaseMethod.destroy();
begin
   _function.destroy();
end;

procedure TSolveOpenEquationBaseMethod.setData(func : string; x0: real);
begin
   _function.solveExpression(func);
   _x0 := x0;
   _error := 0.001;
end;

function TSolveOpenEquationBaseMethod.f(x: real; auxCalculator: Tcalculator):real;
begin
  Result:=(auxCalculator.solveSavedExpression(['x'],[x])).[0][0];
end;

function TSolveOpenEquationBaseMethod.continuo(x: real; auxCalculator: Tcalculator):Boolean;
var t : real;
begin
     Try
       t:= f(x,auxCalculator);
       Result := True;
     Except
       Result:=False;
     end;
end;

function TSolveOpenEquationBaseMethod.fDerivative(x : real; cal :TCalculator) : real;
var
    fxh,fx,h:Real;
begin
     h := 0.0000001;
     fx := f(x,cal);
     x := x+h;
     fxh := f(x,cal);
     Result := (fxh-fx)/h;
end;

function TSolveOpenEquationBaseMethod.solve() : real;
var   Ea, xn, xnPrev, n : real;
begin
  xn := _x0;
  n := 0;
  Ea:=100;
  if continuo(_x0,_function) then
   begin
      while (Ea > _error) AND continuo(xn, _function) AND (n < 20) do
         begin
            if f(xn,_function) = 0 then exit(xn);
            if check0Derivative(xn) then
               begin
                  ShowMessage('La pendiente es 0 ');
                  exit(nan);
               end;
            xnPrev := xn;
            xn := calNextX(xn);
            if (n > 0) then Ea:= xn - xnPrev;
            n:=n+1 ;
         end;
      exit(xn);
   end
  else
     exit(nan);
end;

end.

