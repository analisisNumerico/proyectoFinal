unit FalsePositionMethod;

{$mode objfpc}{$H+}

interface

uses
   SolveCloseEquationBaseMethod;

type
   TFalsePositionMethod = class(TSolveCloseEquationBaseMethod)
   public
      function calNextX(args: array of real) : real; override;
   end;

implementation
function TFalsePositionMethod.calNextX(args: array of real) : real;
begin
  Result:= ((f(args[1],_function)*args[0]) -(f(args[0],_function)*args[1]))/(f(args[1],_function)-f(args[0],_function));
end;
end.
