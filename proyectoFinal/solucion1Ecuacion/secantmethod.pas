unit secantMethod;

{$mode objfpc}{$H+}

interface

uses
   math, SolveOpenEquationBaseMethod;

type
   TSecantMethod = class(TSolveOpenEquationBaseMethod)
   public
      h : real;
      function calNextX(xn : real) : real; override;
      function check0Derivative(x: real): boolean; override;
   end;

implementation

function TSecantMethod.calNextX(xn :real) : real;
var xni : real;
begin
   h:= 0.01;
   try
      xni := xn-(((2*h*f(xn,_function)))/(f(xn+h,_function)-f(xn-h,_function)));
   Except;
      xni := Infinity;
   end;
   Result:= xni;
end;

function TSecantMethod.check0Derivative(x: real) : boolean;
begin
  Result:= false;
end;


end.

