unit NewtonMethod;

{$mode objfpc}{$H+}

interface

uses
   SolveOpenEquationBaseMethod;

type
   TNewtonMethod = class(TSolveOpenEquationBaseMethod)
   public
      function calNextX(x: real) : real; override;
      function check0Derivative(x: real): Boolean; override;
   end;

implementation

function TNewtonMethod.calNextX(x : real) : real;
begin
  Result := x-(f(x,_function)/fDerivative(x,_function));
end;


function TNewtonMethod.check0Derivative(x:  real) : boolean;
begin
  Result := round(fDerivative(x,_function))=0;
end;

end.
