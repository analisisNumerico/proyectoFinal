unit bisectionmethod;

{$mode objfpc}{$H+}

interface

uses
   SolveCloseEquationBaseMethod;

type
   TBisectionMethod = class(TSolveCloseEquationBaseMethod)
   public
      function calNextX(args: array of real) : real; override;
   end;

implementation
function TBisectionMethod.calNextX(args: array of real) : real;
begin
  Result:= (args[0]+args[1])/2;
end;
end.
