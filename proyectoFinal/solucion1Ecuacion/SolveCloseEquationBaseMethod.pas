unit SolveCloseEquationBaseMethod;

{$mode objfpc}{$H+}

interface

uses
    mCalculator, Dialogs;

type
   TSolveCloseEquationBaseMethod = class
   protected
      _function : TCalculator;
      _x0 : real;
      _xf : real;
      _error : real;
   public
      constructor create();
      destructor destroy(); override;

      procedure setData(func : string; x0, xf: real);
      function solve() : real;
      function calNextX(args: array of real) : real; virtual; abstract;
      function f(x: real; auxCalculator: Tcalculator) : real;
      function continuo(x: real; auxCalculator: Tcalculator) : boolean;
   end;

implementation

constructor TSolveCloseEquationBaseMethod.create();
begin
   _function := TCalculator.create();
end;

destructor TSolveCloseEquationBaseMethod.destroy();
begin
   _function.destroy();
end;

procedure TSolveCloseEquationBaseMethod.setData(func : string; x0, xf: real);
begin
   _function.solveExpression(func);
   _x0 := x0;
   _xf := xf;
   _error := 0.0001;
end;
function TSolveCloseEquationBaseMethod.f(x: real; auxCalculator: Tcalculator):real;
begin
  Result:=(auxCalculator.solveSavedExpression(['x'],[x])).[0][0];
end;

function TSolveCloseEquationBaseMethod.continuo(x: real; auxCalculator: Tcalculator):Boolean;
var t : real;
begin
     Try
       t:= f(x,auxCalculator);
       Result := True;
     Except
       Result:=False;
     end;
end;

function TSolveCloseEquationBaseMethod.solve() : real;
var
  Ea, Er, xnPrev, xn : real  ;
  n : integer;
begin
   if continuo(_x0, _function)= False or continuo(_xf,_function)= False then
      begin
      ShowMessage('f(x) no es continua en uno de los intervalos');
      exit(-1);
      end
   else
      begin
      if  f(_x0, _function)*f(_xf, _function) >= 0 then
         begin
         if f(_x0, _function) = 0 then exit(_x0);
         if f(_xf, _function) = 0 then exit(_xf);

         if f(_x0, _function)*f(_xf,_function)>0 then
            begin
            ShowMessage('No cumple bolzano en el intervalo');
            exit(-1);
            end
         end
      else
         begin

         Er := 100;
         Ea := 100;
         n := 0;//numero de iteraciones
         xn := 0;
         xnPrev:=0;
         while (Er > _error) AND (Ea > _error) AND continuo(_x0, _function) AND continuo(_xf, _function)do
            begin
            xn := calNextX([_x0,_xf]);
            if n > 0 then
               begin
               Ea := abs(xn- xnPrev);
               Er := abs((xn-_xf)/ xn);
               {Para ver el procedimiento}
               //ShowMessage(' n: '+ FloatToStr(n)+' a: '+ FloatToStr(a)+' b: '+ FloatToStr(b)+' xn:'+ FloatToStr(xn)+ ' Ea: '+ FloatToStr(Ea)+' Er: '+ FloatToStr(Er));
               end;
            if ( f(xn, _function)*f(_x0, _function) ) <= 0 then
               _xf:= xn
            else
               _x0:= xn;
            n:=n+1;
            xnPrev:=xn;
            end;
         Result := xn;
         end;
      end;
end;

end.

