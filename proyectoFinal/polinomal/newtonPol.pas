unit newtonPol;

{$mode objfpc}{$H+}

interface

uses
   math, sysutils, strutils, Classes;

type
   TArrofArrReal = array of array of real;

   TNewtonPol = class
   private
      pointsXY : TArrofArrReal;
      dividedDiff : array of real;
      ddOldData : array of real;
      n : integer;
      function basisPol(i : integer; x : real) : real;

   public
      constructor create();

      function eval (x : real) : real;
      procedure setData(ptsXY : TArrofArrReal);
      procedure addValue (x , fx : real);
      function solve(): string ;
   end;

implementation

constructor TNewtonPol.create();
begin
   setLength(pointsXY, 0, 0);
   setLength(dividedDiff, 0);
   n := 0;
end;

procedure TNewtonPol.setData(ptsXY : TArrofArrReal);
var
   i, j, cnt: integer;
begin
   pointsXY := ptsXY;
   n := length(ptsXY);

   setLength(ddOldData, n);
   setLength(dividedDiff, n);
   for i := 0 to n - 1 do
      ddOldData[i] := pointsXY[i][1];

   i := n - 2;
   cnt := 1;
   dividedDiff[0] := ddOldData[0];
   while (i >= 0) do
   begin
      for j := 0 to i do
         ddOldData[j] := (ddOldData[j + 1] - ddOldData[j]) / (pointsXY[j + cnt][0] - pointsXY[j][0]);
      dividedDiff[n - 1 - i] := ddOldData[0];
      cnt := cnt + 1;
      i := i - 1;

   end;
end;

function TNewtonPol.basisPol(i : integer; x : real) : real;
var
   j : integer;
   res : real;
begin
   if (i = 0) then
      exit(1);

   res := 1;
   for j := 0 to i - 1 do
      res := res * (x - pointsXY[j][0]);
   Result := res;
end;

function TNewtonPol.eval (x : real) : real;
var
   i : integer;
   res : real;
begin
   res := 0;
   for i := 0 to n - 1 do
      res := res + dividedDiff[i]* self.basisPol(i, x);
   Result := res;
end;

procedure TNewtonPol.addValue(x , fx : real);
var
   i : integer;
begin
   n := n + 1;
   setLength(pointsXY, n ,2);
   setLength(ddOldData, n);
   setLength(dividedDiff, n);

   pointsXY[n - 1][0] := x;
   pointsXY[n - 1][0] := fx;

   ddOldData[n - 1] := fx;
   i := n - 2;
   while (i >= 0) do
   begin
      ddOldData[i] := (ddOldData[i + 1] - ddOldData[i]) /  (pointsXY[n - 1][0] - pointsXY[i][0]);
      i := i - 1;
   end;
   dividedDiff[n - 1] := ddOldData[0];
end;

function TNewtonPol.solve(): string ;
var
   i,j,q : integer;
   equation ,eq_parse : string;
   terminos : TStringArray;
   num_dh: integer;

begin
   num_dh := length(pointsXY[0]);
   SetLength(terminos,num_dh);
   eq_parse:='';
   for j:=0 to num_dh-2 do      //calculo de las diferencias divididas
   begin
      for i:= num_dh-1 downto j+1 do
      begin
         pointsXY[1][i]:=(pointsXY[1][i]-pointsXY[1][i-1])/(pointsXY[0][i]-pointsXY[0][i-j-1]);
      end;
   end;

   for i:= num_dh-1 downto 0 do
   begin
      equation:='';
      for j:=0 to i-1 do
      begin
         equation:=equation+'(x-'+FloatToStr(pointsXY[0][j])+')*';
      end;
      j:=j+1;
      if(j=1) and (i=0) then j:=0;
      terminos[j]:=equation;
   end;

   for q:=0 to num_dh-1 do
   begin
      eq_parse:=eq_parse+'('+terminos[q]+FloatToStr(pointsXY[1][q])+')';
      if q < num_dh-1 then eq_parse:=eq_parse+'+';
   end;
   Result := eq_parse; //polinomio de Newton
end;

end.
