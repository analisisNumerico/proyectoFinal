unit lagrangePol;

{$mode objfpc}{$H+}

interface

uses
    sysutils, Classes;

type
   TArrofArrReal = array of array of real;
   TLagrangePol = class
   private
      pointsXY : TArrofArrReal;
      n : integer;
      function basisPol(i : integer; x : real) : real;

   public
      constructor create();

      function eval(x : real) : real;
      procedure setData(ptsXY : TArrofArrReal);
      function solve(): string;
   end;

implementation

constructor TLagrangePol.create();
begin
   setLength(pointsXY, 0, 0);
   n := 0;
end;

procedure TLagrangePol.setData(ptsXY : TArrofArrReal);
begin
   pointsXY := ptsXY;
   n := length(ptsXY);
end;

function TLagrangePol.basisPol(i : integer; x : real) : real;
var
   j : integer;
   res : real;
begin
   res := 1;
   for j := 0 to n - 1 do
   begin
      if (j <> i) then
         res := res * ((x - pointsXY[j][0])
                       / (pointsXY[i][0] - pointsXY[j][0]));
   end;
   Result := res;
end;

function TLagrangePol.eval (x : real) : real;
var
   i : integer;
   res : real;
begin
   res := 0;
   for i := 0 to n - 1 do
      res := res + pointsXY[i][1]* self.basisPol(i, x);

   Result := res;
end;

function TLagrangePol.solve(): string ;
var
  num_dh : integer; //numero de datos historicos
  i, j, q : integer;
  equation, eq_parse : string;
  terms :TStringArray;
begin
     num_dh := length(pointsXY[0]);
     SetLength(terms,num_dh);
     eq_parse:='';

     for j:= 0 to num_dh - 1 do
        begin
        equation:='';
        q:= 0;
        for i:= 0 to num_dh - 1 do
           begin
           if j<>i then
              begin
              equation := equation + '((x-' + FloatToStr(pointsXY[0][i]) + ')/(' + FloatToStr(pointsXY[0][j]) + '-' + FloatToStr(pointsXY[0][i]) + '))';
              if (q < num_dh-2)  then equation := equation + '*';
              q := q + 1;
              end;
           end;
        terms[j]:=equation;
        end;

      for q := 0 to num_dh - 1 do
         begin
         eq_parse := eq_parse + '('+ FloatToStr(pointsXY[1][q]) + '*(' + terms[q] + '))';
         if q < num_dh-1 then eq_parse := eq_parse + '+';
         end;
      Result := eq_parse; //polinomio de Lagrange
  end;

end.
