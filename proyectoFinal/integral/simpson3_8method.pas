unit Simpson3_8Method;

{$mode objfpc}{$H+}

interface

uses
   math, mCalculator, mFuncImp, mMatrix;

type
   TSimpson3_8Method = class
   public
      _function : TCalculator;
      _low : real;
      _high : real;
      _n : integer;
   public
      constructor create();
      destructor destroy(); override;

      procedure setData(func : string; low, high : real);
      function solve() : real;
   end;

implementation

constructor TSimpson3_8Method.create();
begin
   _function := TCalculator.create();
end;

destructor TSimpson3_8Method.destroy();
begin
   _function.destroy();
end;

procedure TSimpson3_8Method.setData(func : string; low, high : real);
begin
   _function.solveExpression(func);
   _low := low;
   _high := high;
   _n := 21; // Aquí está la cantidad de subintervalos
end;

function TSimpson3_8Method.solve() : real;
var
   h, xi, sum1, sum2, sum3, aux : real;
   i : integer;
begin
   h := (_high - _low) / _n; // Aquí calculo el tamaño de cada subintervalo
   sum1 := 0;
   sum2 := 0;
   sum3 := 0;

   i := 1;
   while i <= _n - 2 do
   begin
      xi := _low + i * h;
      sum1 := sum1 + _function.solveSavedExpression(['x'], [xi])[0][0];
      i += 3;
   end;

   i := 2;
   while i <= _n - 1 do
   begin
      xi := _low + i * h;
      sum2 := sum2 + _function.solveSavedExpression(['x'], [xi])[0][0];
      i += 3;
   end;

   i := 3;
   while i <= _n - 3 do
   begin
      xi := _low + i * h;
      sum3 := sum3 + _function.solveSavedExpression(['x'], [xi])[0][0];
      i += 3;
   end;

   aux := _function.solveSavedExpression(['x'], [_low])[0][0] +
      _function.solveSavedExpression(['x'], [_high])[0][0];
   aux := aux + 3 * sum1 + 3 * sum2 + 2 * sum3;
   aux := aux * 3 * h / 8;
   Result := aux;
end;

end.
