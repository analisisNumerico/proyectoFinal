unit TrapezoidalMethod;

{$mode objfpc}{$H+}

interface

uses
   math, mCalculator, mFuncImp, mMatrix;

type
   TArrStr = array of string;

   TTrapezoidalMethod = class
   public
      _function : TCalculator;
      _low : real;
      _high : real;
      _n : integer;
   public
      constructor create();
      destructor destroy(); override;
      procedure setData(func : string; low, high : real);
      function solve() : real;
   end;

implementation

constructor TTrapezoidalMethod.create();
begin
   _function := TCalculator.create();
end;

destructor TTrapezoidalMethod.destroy();
begin
   _function.destroy();
end;

procedure TTrapezoidalMethod.setData(func : string; low, high : real);
begin
   _function.solveExpression(func);
   _low := low;
   _high := high;
   _n :=  20; // Aquí está la cantidad de subintervalos
end;

function TTrapezoidalMethod.solve() : real;
var
   h, xi, sum, aux : real;
   i : integer;
begin
   h := (_high - _low) / _n; // Aquí calculo el tamaño de cada subintervalo
   sum := 0;
   for i := 1 to _n - 1 do
   begin
      xi := _low + i * h;
      sum := sum + _function.solveSavedExpression(['x'], [xi])[0][0];
   end;
   aux := _function.solveSavedExpression(['x'], [_low])[0][0] +
      _function.solveSavedExpression(['x'], [_high])[0][0];
   aux := aux / 2 + sum;
   aux := aux * h;
   Result := aux;
end;

end.
